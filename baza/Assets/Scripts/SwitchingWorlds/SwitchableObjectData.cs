﻿using UnityEngine;
using System;

namespace SwitchingWorlds
{
    [CreateAssetMenu(fileName = "SwitchableObjectData", menuName = "ObjectData/SwitchableObjectData", order = 1)]
    public class SwitchableObjectData : ScriptableObject
    {
        [NonSerialized] public HasObject hasObjectsMask;

        public WorldData humanWorld;
        public WorldData spiritWorld;

        [Serializable]
        public struct WorldData
        {
            public Sprite worldSprite;
            public Material worldMaterial;
            public Vector3 worldScale;
        }

        private void OnValidate()
        {
            hasObjectsMask = HasObject.NONE;

            if (humanWorld.worldSprite != null && spiritWorld.worldSprite != null)
            {
                hasObjectsMask |= HasObject.SPRITE;
                hasObjectsMask |= ~HasObject.NONE;
            }
            if (humanWorld.worldMaterial != null && spiritWorld.worldMaterial != null)
            {
                hasObjectsMask |= HasObject.MATERIAL;
                hasObjectsMask |= ~HasObject.NONE;
            }
            if (humanWorld.worldScale != Vector3.zero && spiritWorld.worldScale != Vector3.zero)
            {
                hasObjectsMask |= HasObject.SCALE;
                hasObjectsMask |= ~HasObject.NONE;
            }
        }

        [Flags]
        public enum HasObject
        {
            NONE = 0,
            SPRITE = 1,
            MATERIAL = 2,
            SCALE = 4
        }
    }
}
