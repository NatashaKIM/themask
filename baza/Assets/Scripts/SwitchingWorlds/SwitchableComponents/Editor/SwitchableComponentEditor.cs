﻿using UnityEngine;
using UnityEditor;

namespace SwitchingWorlds
{

    [CustomEditor(typeof(SwitchableComponent), editorForChildClasses: true)]
    [CanEditMultipleObjects]
    public class SwitchableComponentEditor : Editor
    {
        SwitchableComponent switchableComponent;
        SwitchableObject switchableObject;

        int index = 0;

        public override void OnInspectorGUI()
        {
            switchableComponent = target as SwitchableComponent;
            switchableObject = switchableComponent.GetComponentInParent<SwitchableObject>();

            if (switchableObject == null)
            {
                EditorGUILayout.LabelField("Parent has to have SwitchableObject added!");
                EditorGUILayout.Space(25);

                if (GUI.Button(new Rect(50, 25, 100, 25), "Add to parent"))
                {
                    switchableObject = switchableComponent.transform.parent.gameObject.AddComponent<SwitchableObject>();
                    switchableObject.AddSwitchableComponent(switchableComponent);
                }
            }
            else
            { 
                index = switchableObject.GetIndex(switchableComponent);
                
                if (switchableObject.ContainsSwitchableComponent(switchableComponent))
                {
                    EditorGUILayout.LabelField("Succesfully added to parent!");
                    EditorGUILayout.Space(25);
                
                    if(GUI.Button(new Rect(50, 25, 100, 25), "Remove"))
                    {
                        switchableObject.RemoveSwitchableComponent(switchableComponent);
                    }
                }
                else
                {
                    EditorGUILayout.LabelField("Cannot add to parent or removed!");
                    EditorGUILayout.Space(25);
                
                    if (GUI.Button(new Rect(50, 25, 100, 25), "Try to add"))
                    {
                        switchableObject.AddSwitchableComponent(switchableComponent);
                    }
                }

            }

            EditorGUILayout.Space();
            DrawDefaultInspector();
        }

        // OnComponentRemoved event
        private void OnDisable()
        {
            if (target == null)
            {
                switchableObject?.RemoveSwitchableComponentAt(index);
            }
        }
    }
}
