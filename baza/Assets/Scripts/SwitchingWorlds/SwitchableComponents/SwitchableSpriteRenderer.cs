﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace SwitchingWorlds
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SwitchableSpriteRenderer : SwitchableComponent
    {
        [SerializeField] private Sprite humanWorldSprite;
        [SerializeField] private Sprite spiritWorldSprite;

        private new SpriteRenderer renderer;

        protected override void Awake()
        {
            base.Awake();
            renderer = this.GetComponent<SpriteRenderer>();
        }

        public override void Switch(World currentWorld)
        {
            switch (currentWorld)
            {
                case World.HUMAN:
                    renderer.sprite = humanWorldSprite;
                    break;
                case World.SPIRIT:
                    renderer.sprite = spiritWorldSprite;
                    break;
            }
        }
    }
}
