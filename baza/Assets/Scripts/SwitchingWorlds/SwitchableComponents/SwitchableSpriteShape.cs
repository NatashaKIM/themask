﻿using UnityEngine;
using UnityEngine.U2D;

namespace SwitchingWorlds
{
    [RequireComponent(typeof(SpriteShapeController))]
    public class SwitchableSpriteShape : SwitchableComponent
    {
        [SerializeField] private SpriteShape humanWorldProfile;
        [SerializeField] private SpriteShape spiritWorldProfile;

        SpriteShapeController shapeController;

        protected override void Awake()
        {
            base.Awake();
            shapeController = GetComponent<SpriteShapeController>();
        }

        public override void Switch(World currentWorld)
        {
            switch (currentWorld)
            {
                case World.HUMAN:
                    shapeController.spriteShape = humanWorldProfile;
                    break;
                case World.SPIRIT:
                    shapeController.spriteShape = spiritWorldProfile;
                    break;
            }

            shapeController.RefreshSpriteShape();
        }
    }
}
