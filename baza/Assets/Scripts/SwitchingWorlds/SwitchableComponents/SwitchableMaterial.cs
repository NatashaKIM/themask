﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace SwitchingWorlds
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SwitchableMaterial : SwitchableComponent
    {
        [SerializeField] private Material humanWorldMaterial;
        [SerializeField] private Material spiritWorldMaterial;

        private new SpriteRenderer renderer;

        protected override void Awake()
        {
            base.Awake();
            renderer = this.GetComponent<SpriteRenderer>();
        }

        public override void Switch(World currentWorld)
        {
            switch (currentWorld)
            {
                case World.HUMAN:
                    renderer.sharedMaterial = humanWorldMaterial;
                    break;
                case World.SPIRIT:
                    renderer.sharedMaterial = spiritWorldMaterial;
                    break;
            }
        }
    }
}
