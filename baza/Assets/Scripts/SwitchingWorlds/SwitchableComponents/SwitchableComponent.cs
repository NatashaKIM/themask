﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwitchingWorlds
{
    public abstract class SwitchableComponent : MonoBehaviour
    {
        protected SwitchableObject parent;

        public abstract void Switch(World currentWorld);

        protected virtual void Awake()
        {
            if(parent == null)
            {
                parent = GetComponentInParent<SwitchableObject>();

                if (parent == null) return;

                if(!parent.ContainsSwitchableComponent(this))
                    parent.AddSwitchableComponent(this);
            }
            else
            {
                if(!parent.ContainsSwitchableComponent(this))
                    parent.AddSwitchableComponent(this);
            }
        }
    }
}
