﻿using UnityEngine;
namespace SwitchingWorlds
{
    public class SwitchableGameObjectEnabler : SwitchableComponent
    {
        [SerializeField] private bool isEnabledInSpirit = true;

        public override void Switch(World currentWorld)
        {
            switch (currentWorld)
            {
                case World.HUMAN:
                    gameObject.SetActive(!isEnabledInSpirit);
                    break;
                case World.SPIRIT:
                    gameObject.SetActive(isEnabledInSpirit);
                    break;
            }
        }
    }
}
