using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventsBus;

namespace SwitchingWorlds
{
    public class SwitchableObject : MonoBehaviour, ISwitchable, IListener<OnWorldsChanged>
    {
        private IEventBus eventBus;

        [SerializeField] private float minDistanceToCamera = 15f;
        [SerializeField] private int maxFramesToPass = 60;

        private SpriteRenderer spriteRenderer;

        private int currentFrame = 0;
        private int frameToWait = 1;
        private bool isSwitching = false;
        private Coroutine switchCoroutine = null;
        private List<SwitchableComponent> switchableComponents = new List<SwitchableComponent>();

        private void Awake()
        {
            StartCoroutine(Inject());
        }

        private IEnumerator Inject()
        {
            yield return 0;
            (GameObject.FindObjectOfType(typeof(Injector)) as Injector).Inject(this);
        }

        public void Init(IEventBus eventBus)
        {
            eventBus.RegisterListener(this);

            switchableComponents.Clear();

            foreach (Transform t in transform)
            {
                if(t.TryGetComponent(typeof(SwitchableComponent), out Component component))
                {
                    switchableComponents.Add(component as SwitchableComponent);
                }
                if(t.TryGetComponent(typeof(SwitchKey), out component))
                {
                    component.gameObject.SetActive(true);
                }
                if(t.TryGetComponent(typeof(Door), out component))
                {
                    component.gameObject.SetActive(true);
                }
            }
        }

        public void OnEvent(OnWorldsChanged data)
        {
            //if (Vector3.Distance(data.MainCamera.transform.position, this.transform.position) < minDistanceToCamera)
            //{
            ChangeVisualsImmidiately(data.CurrentWorld);
            //}
            //else
            //{
            //    if(switchCoroutine == null)
            //        switchCoroutine = StartCoroutine(ChangeVisuals(data.CurrentWorld));
            //    else
            //    {
            //        StopCoroutine(switchCoroutine);
            //        switchCoroutine = StartCoroutine(ChangeVisuals(data.CurrentWorld));
            //    }
            //}
        }

        private void ChangeVisualsImmidiately(World currentWorld)
        {
            for (int i = 0; i < switchableComponents.Count; i++)
            {
                switchableComponents[i].Switch(currentWorld);
            }
        }

        private IEnumerator ChangeVisuals(World currentWorld)
        {
            int framesToWait = Random.Range(1, maxFramesToPass + 1);
            yield return new WaitUntil(() => --framesToWait < 0);

            for (int i = 0; i < switchableComponents.Count; i++)
            {
                switchableComponents[i].Switch(currentWorld);
            }
        }

        #region SwitchableComponents
        public void AddSwitchableComponent(SwitchableComponent component)
        {
            if (!ContainsSwitchableComponent(component))
                switchableComponents.Add(component);
            //else
            //  throw new ArgumentException(string.Format("Component {0} already added!", component));
        }

        public void RemoveSwitchableComponent(SwitchableComponent component)
        {
            switchableComponents.Remove(component);
        }

        public void RemoveSwitchableComponentAt(int index)
        {
            if(index < switchableComponents.Count)
                switchableComponents.RemoveAt(index);
        }

        public int GetIndex(SwitchableComponent component)
        {
            return switchableComponents.FindIndex(x => x == component);
        }

        public bool ContainsSwitchableComponent(SwitchableComponent component)
        {
            return switchableComponents.Contains(component);
        }
        #endregion
    }
}
