using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour, IInputHandler
{
    public bool isJumping { get; private set; }
    public bool isHoldingJumpingButton { get; private set; }
    public bool isAttacking { get; private set; }
    public bool isSpecialAttacking { get; private set; }
    public bool isDashing { get; private set; }
    public bool switchWorlds { get; private set; }
    public float MoveValue { get; private set; }

    private void Update()
    {
        MoveValue = Input.GetAxisRaw("Horizontal");
        isJumping = Input.GetButtonDown("Jump");
        isHoldingJumpingButton = Input.GetButton("Jump");
        isAttacking = Input.GetButtonDown("Fire1");
        isSpecialAttacking = Input.GetButtonDown("Fire2");
        isDashing = Input.GetKeyDown(KeyCode.G);
        switchWorlds = Input.GetKeyDown(KeyCode.C);
    }
}
