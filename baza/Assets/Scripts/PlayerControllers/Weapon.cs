using UnityEngine;
using EventsBus;

public class Weapon : MonoBehaviour
{
    private IEventBus eventBus; 

    [SerializeField] private int damage = 2;
    [SerializeField] private float showTimeInterval = 2f;
    public bool isAnimating = false;
    private Vector3 startingRotation = new Vector3(0, 0, 80);

    float currentShowTimeInterval = 0;
    float timeFinished = 0;

    public void Init(IEventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    public void AnimateAttack()
    {
        gameObject.SetActive(true);
        currentShowTimeInterval = Time.time;
        timeFinished = Time.time + showTimeInterval;
        isAnimating = true;
    }

    private void Update()
    {
        if (isAnimating)
        {
            if(currentShowTimeInterval < timeFinished)
            {
                currentShowTimeInterval += Time.deltaTime;
            }
            else
            {
                isAnimating = false; 
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            eventBus.InvokeEvent(new OnWeaponHitted(other, damage));
        }
    }
}
