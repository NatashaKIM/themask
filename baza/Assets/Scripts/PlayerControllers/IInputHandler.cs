﻿public interface IInputHandler
{
    public bool isJumping { get; }
    public bool isHoldingJumpingButton { get; }
    public bool isAttacking { get; }
    public bool isSpecialAttacking { get; }
    public bool isDashing { get; }
    public bool switchWorlds { get; }
    public float MoveValue { get; }
}
