using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private GameObject idleState;
    [SerializeField] private GameObject runState;
    [SerializeField] private GameObject jumpState;
    [SerializeField] private GameObject attackState;

    private GameObject currentStateObject;
    private AnimationState currentState = AnimationState.NONE; 

    private void Awake()
    {

        SetState(AnimationState.IDLE);
    }

    public void SetState(AnimationState state)
    {
        if (currentState == state) return;

        currentStateObject?.SetActive(false);
        switch (state)
        {
            case AnimationState.IDLE:
                currentStateObject = idleState;
                currentState = AnimationState.IDLE;
                break;
            case AnimationState.RUN:
                currentStateObject = runState;
                currentState = AnimationState.RUN;
                break;
            case AnimationState.ATTACK:
                currentStateObject = attackState;
                currentState = AnimationState.ATTACK;
                break;
            case AnimationState.JUMP:
                currentStateObject = jumpState;
                currentState = AnimationState.JUMP;
                break;
        }

        currentStateObject.SetActive(true);

        if(currentStateObject.TryGetComponent(typeof(Animator), out Component anim))
        {
            Animator animator = anim as Animator;
            animator.SetTrigger("Animate");
        }
    }

}
    public enum AnimationState
    {
        NONE,
        IDLE,
        RUN,
        JUMP,
        ATTACK
    }
