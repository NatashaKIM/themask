﻿using System;
using UnityEngine;
using EventsBus;
using UIViews;

public class PlayerController : MonoBehaviour, IPlayerController, IUpdatable, IListener<OnUnlockDoubleJump>, IListener<OnUnlockDash>,
    IListener<OnWeaponHitted>, IListener<OnEnemyDied>
{
    private const float GROUNDED_RADIUS = .2f;
    private const float CEILING_RADIUS = .2f;

    private IEventBus eventBus;
    private IInputHandler inputHandler;
    private IUIController uIController;

    [Header("Movement")]
    [Range(0, 1), SerializeField] private float crouchSpeed = .36f;
    [Range(0, 0.3f), SerializeField] private float movementSmoothing = .05f;
    [SerializeField] private float movementSpeed = 1;
    [SerializeField] private float dashVelocityMultiplier = 7f;
    [SerializeField] private float dashDelay = 20f;
    [Range(7, 25), SerializeField] private float jumpVelocity = 14;
    [SerializeField] private float fallMultiplier = 2.5f;
    [SerializeField] private float lowJumpMultiplier = 2f;
    [SerializeField] private int possibleJumpsCount = 2;
    [SerializeField] private float doubleJumpDelay = 1f;
    [SerializeField] private bool airControl = false;
    [SerializeField] private LayerMask groundLayerMask;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private Transform ceilingCheck;
    [SerializeField] private Collider2D crouchDisableCollider;

    [Header("Attack")]
    [SerializeField] private Weapon weapon;
    [SerializeField] private int healthPoints = 50;
    [SerializeField] private int healthPointsToAddOnEnemyDeath = 15;

    private PlayerAnimation playerAnimation;

    private Rigidbody2D rb;
    private Vector3 velocity = Vector3.zero;

    private bool inited;
    private bool isGrounded;
    private bool canDash;
    private World currentWorld;

    private int currentHealthPoints;
    private bool isFacingRight = true;
    private int currentPossibleJumpCount = 1;
    private int currentJump = 0;

    private DashState dashState;
    private float dashTimer;

    private Vector2 savedVelocity;

    private Action swapXOffset = null;

    public bool CanMove;
    public bool died;


    public void Init(IEventBus eventBus, IInputHandler inputHandler, ICameraController camera, IUIController uIController)
    {
        rb = GetComponent<Rigidbody2D>();

        this.eventBus = eventBus;
        this.inputHandler = inputHandler;
        this.uIController = uIController;
        swapXOffset = camera.SwapXOffset;
        currentHealthPoints = healthPoints;

        eventBus.RegisterListener<OnUnlockDoubleJump>(this);
        eventBus.RegisterListener<OnUnlockDash>(this);
        eventBus.RegisterListener<OnWeaponHitted>(this);
        eventBus.RegisterListener<OnEnemyDied>(this);

        GetComponentInChildren<Weapon>(true).Init(eventBus);

        playerAnimation = GetComponent<PlayerAnimation>();
        inited = true;
        CanMove = true;
    }

    public void UpdateComponent()
    {
        HandleAttack();
        HandleJump();
        HandleDash();
        HandleMask();

        if (inputHandler.isAttacking || weapon.isAnimating)
        {
            playerAnimation.SetState(AnimationState.ATTACK);
        }
        else if(!IsInRange(rb.velocity.y, -0.2f, 0.2f) && !isGrounded)
        {
            playerAnimation.SetState(AnimationState.JUMP);
        }
        else if(!IsInRange(rb.velocity.x, -0.2f, 0.2f) && isGrounded)
        {
            playerAnimation.SetState(AnimationState.RUN);
        }
        else
        {
            playerAnimation.SetState(AnimationState.IDLE);
        }
    }

    private bool IsInRange(float value, float min, float max)
    {
        return value < max && value > min;
    }


    private void FixedUpdate()
    {
        if (!inited || !CanMove) return;

        bool wasGrounded = isGrounded;
        isGrounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, GROUNDED_RADIUS, groundLayerMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                isGrounded = true;
                if(!wasGrounded)    
                    currentJump = 0;
            }
        }

        Move(inputHandler.MoveValue);
    }


    private void Move(float move)
    {
        if (isGrounded || airControl)
        {
            Vector3 targetVelocity = new Vector2(move * 10f, rb.velocity.y);
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, movementSmoothing);

            if (move > 0 && !isFacingRight)
            {
                Flip();
            }
            else if (move < 0 && isFacingRight)
            {
                Flip();
            }
        }
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        swapXOffset?.Invoke();
    }

    private void HandleAttack()
    {
        if (inputHandler.isAttacking)
        {
            weapon.AnimateAttack();
        }
    }

    private void HandleJump()
    {
        if (inputHandler.isJumping)
        {
            if (currentJump < currentPossibleJumpCount)
            {
                currentJump++;
                rb.velocity = new Vector2(rb.velocity.x, jumpVelocity);
            }
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !inputHandler.isHoldingJumpingButton)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    private void HandleDash()
    {
        if (canDash)
        {
            switch (dashState)
            {
                case DashState.Ready:
                    if (inputHandler.isDashing)
                    {
                        savedVelocity = rb.velocity;
                        rb.velocity = new Vector2(rb.velocity.x * dashVelocityMultiplier, rb.velocity.y);
                        dashState = DashState.Dashing;
                    }
                    break;
                case DashState.Dashing:
                    dashTimer += Time.deltaTime * 3;
                    if (dashTimer >= dashDelay)
                    {
                        dashTimer = dashDelay;
                        rb.velocity = savedVelocity;
                        dashState = DashState.Cooldown;
                    }
                    break;
                case DashState.Cooldown:
                    dashTimer -= Time.deltaTime;
                    if (dashTimer <= 0)
                    {
                        dashTimer = 0;
                        dashState = DashState.Ready;
                    }
                    break;
            }
        }
    }

    private void HandleMask()
    {
        if (inputHandler.switchWorlds)
        {
            if(currentWorld == World.HUMAN)
            {
                currentWorld = World.SPIRIT;
            }
            else
            {
                currentWorld = World.HUMAN;
            }

            eventBus.InvokeEvent(new OnWorldsChanged(currentWorld)); 
            uIController.UpdateData(new HealthBarViewData((float)currentHealthPoints / (float)healthPoints, currentWorld == World.SPIRIT));
        }
    }

    public void OnEvent(OnUnlockDoubleJump data)
    {
        currentPossibleJumpCount = possibleJumpsCount;
    }

    public void OnEvent(OnUnlockDash data)
    {
        canDash = true;
    }

    public void OnEvent(OnWeaponHitted data)
    {
        if (data.hittedCollider.CompareTag("Player")) // Did it hit player?
        {
            currentHealthPoints -= data.damage;

            uIController.UpdateData(new HealthBarViewData((float)currentHealthPoints / (float)healthPoints, currentWorld == World.SPIRIT));

            if(currentHealthPoints <= 0)
            {
                died = true;
            }
        }
    }

    public void OnEvent(OnEnemyDied data)
    {
        currentHealthPoints += healthPointsToAddOnEnemyDeath;
        currentHealthPoints = currentHealthPoints > healthPoints ? healthPoints : currentHealthPoints;
        uIController.UpdateData(new HealthBarViewData((float)currentHealthPoints / (float)healthPoints, currentWorld == World.SPIRIT));
    }

    enum DashState
    {
        Ready,
        Dashing,
        Cooldown
    }
}