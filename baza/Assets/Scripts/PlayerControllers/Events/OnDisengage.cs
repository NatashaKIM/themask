﻿using UnityEngine;
using ReikonAI;

namespace EventsBus
{
    public class OnDisengage : Event
    {
        public BaseEnemy enemy;

        public OnDisengage(BaseEnemy enemy)
        {
            this.enemy = enemy;
        }
    }
}
