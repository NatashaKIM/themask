﻿using UnityEngine;
using ReikonAI;

namespace EventsBus
{
    public class OnEngage : Event
    {
        public BaseEnemy enemy;

        public OnEngage(BaseEnemy enemy)
        {
            this.enemy = enemy;
        }
    }
}
