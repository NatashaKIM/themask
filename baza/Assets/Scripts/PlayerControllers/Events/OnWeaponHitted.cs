﻿using UnityEngine;

namespace EventsBus
{
    public class OnWeaponHitted : Event
    {
        public Collider2D hittedCollider;
        public int damage = 0;

        public OnWeaponHitted(Collider2D hittedCollider, int damage)
        {
            this.hittedCollider = hittedCollider;
            this.damage = damage;
        }
    }
}