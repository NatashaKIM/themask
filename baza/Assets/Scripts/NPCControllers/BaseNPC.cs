﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;
using FSM.States;

namespace ReikonAI
{
    public abstract class BaseNPC : MonoBehaviour
    {
        protected FiniteStateMachine stateMachine;
        protected IEventBus eventBus;
        protected IPlayerController playerController;

        public virtual void Init(IEventBus eventBus, IPlayerController playerController)
        {
            this.eventBus = eventBus;
            this.playerController = playerController;
        }

        public abstract void CheckTransitions();
    }
}