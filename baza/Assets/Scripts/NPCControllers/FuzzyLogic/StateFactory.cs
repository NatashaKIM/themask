﻿using System;
using FuzzyLogic.States;

namespace FuzzyLogic
{
    public class StateFactory
    {
        public IState GetState<T>(T state) where T : IState
        {
            switch(state)
            {
                case MoveState move:
                    return new MoveState();
            }
            return default(T);
        }
    }
}
