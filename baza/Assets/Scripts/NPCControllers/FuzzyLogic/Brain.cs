﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FuzzyLogic
{
    public class Brain
    {
        private Dictionary<RuleKey, Action> rules = new Dictionary<RuleKey, Action>();
        private DataSet<Func<float>> dataSet = new DataSet<Func<float>>();
        private Dictionary<AnimationCurve, int> indexes = new Dictionary<AnimationCurve, int>();

        private Action prevAction;

        public Brain() { }


        public void AddRule(AnimationCurve curve, Action action, Func<float> correspondingData)
        {
            //if(!rules.ContainsKey(func))
            //{
            //    rules.Add(func, action);
            //}
            if (!rules.ContainsKey(new RuleKey(curve, dataSet.GetDataIndex(correspondingData))))
            {
                RuleKey key = new RuleKey(curve, dataSet.Count -1);
                rules.Add(key, action);
                dataSet.AddData(correspondingData);
                indexes.Add(curve, dataSet.Count - 1);
            }
        }

        public void RemoveRule(AnimationCurve curve)
        {
            int i = indexes[curve];
            RuleKey key = new RuleKey(curve, i);
            rules.Remove(key);
            dataSet.RemoveDataAt(i);
        }

        public void Think()
        {
            List<RuleKey> curves = rules.Keys.ToList();
            Action highestPriorityAction = prevAction;
            float highestPriority = float.PositiveInfinity;

            for (int i = 0; i < curves.Count; i++)
            {
                float currentPriority = curves[i].curve.Evaluate(dataSet.GetData(i).Invoke());
                if(currentPriority < highestPriority)
                {
                    highestPriority = currentPriority;
                    highestPriorityAction = rules[curves[i]];
                }
            }

            if(highestPriorityAction != prevAction)
            {
                prevAction = highestPriorityAction;
                highestPriorityAction.Invoke();
            }
        }

        struct RuleKey
        {
            public AnimationCurve curve;
            public int dataIndex;

            public RuleKey(AnimationCurve curve, int dataIndex)
            {
                this.curve = curve;
                this.dataIndex = dataIndex;
            }
        }
    }
}