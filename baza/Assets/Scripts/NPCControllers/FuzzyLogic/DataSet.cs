﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FuzzyLogic
{
    public class DataSet<T>
    {
        public int Count => data.Count;
        private List<T> data = new List<T>();

        public DataSet(params T[] newData)
        {
            for (int i = 0; i < newData.Length; i++)
            {
                data.Add(newData[i]);
            }
        }

        public void AddData(T toAdd)
        {
            if (!data.Contains(toAdd))
                data.Add(toAdd);
        }

        public void RemoveData(T toRemove)
        {
            if (data.Contains(toRemove))
                data.Remove(toRemove);
        }

        public void RemoveDataAt(int index)
        {
            if (index < data.Count - 1)
                data.RemoveAt(index);
        }

        /// <summary>
        /// Returns object at given index
        /// </summary>
        public T GetData(int index)
        {
            return data.ElementAt(index);
        }

        public int GetDataIndex(T obj)
        {
            return data.IndexOf(obj);
        }
    }
}