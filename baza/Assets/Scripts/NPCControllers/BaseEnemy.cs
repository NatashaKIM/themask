﻿using System.Collections;
using UnityEngine;
using EventsBus;
using FSM;
using FSM.States;

namespace ReikonAI
{
    public class BaseEnemy : BaseNPC, IListener<OnWeaponHitted>
    {
        private const float DISTANCE_TOLERANCE = 0.3f;

        private SpriteRenderer renderer;

        [SerializeField] private int maxHealth = 30;
        [SerializeField] private float moveSpeed = 3f;
        [SerializeField] private float engageDistance = 5f;
        [SerializeField] private float disengageDistance = 8f;
        [SerializeField] private float respawnDelay = 20f;
        [SerializeField] private float animationSpeed = 0.1f;
        [SerializeField] private float maxScale = 3f;
        [SerializeField] private float attackDistance = 1f;
        [SerializeField] private int damage = 5;

        [SerializeField] Transform[] patrolPoints;
        private Coroutine moveCoroutine = null;
        private int currentPatrolIndex = 0;
        private Coroutine animationCoroutine = null;

        private bool isVisible = false;
        private int currentHealth;

        public Sprite normal;
        public Sprite attack;

        private BoxCollider2D collider;


        public override void Init(IEventBus eventBus, IPlayerController playerController)
        {
            base.Init(eventBus, playerController);
            currentHealth = maxHealth;
            renderer = GetComponent<SpriteRenderer>();
            renderer.enabled = true;
            eventBus.RegisterListener<OnWeaponHitted>(this);
            collider = GetComponent<BoxCollider2D>();

            var moveState = new MoveState().SetPerformStateAction(PerformMoveState)
                                           .SetExitStateAction(ExitMoveState);
            var attackState = new AttackState().SetPerformStateAction(PerformAttackState);
            var deathState = new DeathState().SetPerformStateAction(PerformDeathState);

            moveState.AddTransition(new Transition(attackState, CanTransitToAttackState));
            attackState.AddTransition(new Transition(moveState, CanTransitToMoveState));

            stateMachine = new FiniteStateMachine(moveState, deathState);
            stateMachine.TryAddTransitionToAnyState(new Transition(deathState, CanTransitToDeathState));
        }

        public override void CheckTransitions()
        {
            stateMachine.UpdateState();
        }

        #region Conditions
        private bool CanTransitToAttackState()
        {
            if (isVisible && Vector3.Distance(transform.position, playerController.transform.position) <= engageDistance)
            {
                eventBus.InvokeEvent(new OnEngage(this));
                return true;
            }

            return false;
        }

        private bool CanTransitToMoveState()
        {
            if (Vector3.Distance(transform.position, playerController.transform.position) >= disengageDistance)
            {
                eventBus.InvokeEvent(new OnDisengage(this));
                return true;
            }

            return false;
        }

        private bool CanTransitToDeathState()
        {
            if(currentHealth <= 0)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region Actions
        #region MoveState
        private void PerformMoveState()
        {
            if (moveCoroutine == null)
            {
                moveCoroutine = StartCoroutine(Patrol());
            }
        }

        private IEnumerator Patrol()
        {
            if(patrolPoints[currentPatrolIndex].position.x < transform.position.x)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            while(Vector3.Distance(transform.position, patrolPoints[currentPatrolIndex].position) >= DISTANCE_TOLERANCE)
            {
                MoveTowards(patrolPoints[currentPatrolIndex]);
                yield return 0;
            }
            moveCoroutine = null;
            currentPatrolIndex++;
            if (currentPatrolIndex >= patrolPoints.Length) currentPatrolIndex = 0;
        }

        private void ExitMoveState()
        {
            if (moveCoroutine != null) StopCoroutine(moveCoroutine);
            moveCoroutine = null;
        }

        #endregion
        #region AttackState
        private void PerformAttackState()
        {
            if(Vector3.Distance(transform.position, playerController.transform.position) >= attackDistance)
            {
                if (playerController.transform.position.x < transform.position.x)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                }
                MoveTowards(playerController.transform);
            }
            else
            {
                if(animationCoroutine == null)
                {
                    animationCoroutine = StartCoroutine(AnimateAttack()); // Check if hitted in unity messages below
                }
            }
        }

        private IEnumerator AnimateAttack()
        {
            WaitForSeconds delay = new WaitForSeconds(0.3f);

            yield return delay;
            renderer.sprite = attack;
            collider.offset = new Vector2(-0.17f, 0);
            collider.size = new Vector2(3.69f, 3f);
            yield return delay;

            collider.offset = new Vector2(0.7f, 0);
            collider.size = new Vector2(1.89f, 3f);
            renderer.sprite = normal;

            animationCoroutine = null;
        }
        #endregion
        #region DeathState
        private void PerformDeathState()
        {
            eventBus.InvokeEvent(new OnEnemyDied(this));
            eventBus.InvokeEvent(new OnDisengage(this)); // TODO Change to OnEnemyDeath
            this.renderer.enabled = false;
            collider.enabled = false;
            StartCoroutine(Respawn());
        }

        private IEnumerator Respawn()
        {
            yield return new WaitForSeconds(respawnDelay);
            yield return new WaitUntil(() => !isVisible);  // Wait until delay and enemy is not visible and reinit
            collider.enabled = true;
            Init(eventBus, playerController);
        }
        #endregion
        #endregion

        private void MoveTowards(Transform target)
        {
            Vector3 newPos = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            transform.position = newPos;
        }

        public void OnEvent(OnWeaponHitted data)
        {
            if(data.hittedCollider.gameObject == this.gameObject)
            {
                currentHealth -= data.damage;
            }
        }

    #region UnityMessages
        private void OnBecameVisible()
        {
            isVisible = true;
        }

        private void OnBecameInvisible()
        {
            isVisible = false;
        }

        private void OnTriggerEnter2D(Collider2D hittedCollider)
        {
            if (hittedCollider.CompareTag("Player") && renderer.enabled)
            {
                eventBus.InvokeEvent(new OnWeaponHitted(hittedCollider, damage));
            }
        }


#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, engageDistance);
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, disengageDistance);
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(transform.position, attackDistance);
        }
    #endif
    #endregion
    }
}