﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ReikonAI
{
    public class AnimationController : MonoBehaviour
    {
        [SerializeField] private Sprite normal;
        [SerializeField] private Sprite attack;
        [SerializeField] private Weapon weapon;
        private SpriteRenderer renderer;
        private CapsuleCollider2D collider;

        private void Awake()
        {
            renderer = GetComponent<SpriteRenderer>();
            collider = GetComponent<CapsuleCollider2D>();
        }

        public void SetState(AnimationState state)
        {
            switch (state)
            {
                case AnimationState.IDLE:
                    renderer.sprite = normal;
                    renderer.enabled = true;
                    collider.enabled = true;
                    break;
                case AnimationState.ATTACK:
                    renderer.sprite = attack;
                    renderer.enabled = true;
                    collider.enabled = true;
                    break;
            }
        }
    }
}
