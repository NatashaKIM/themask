﻿using System.Collections.Generic;

namespace FSM.States
{
    public interface IState 
    {
        void OnEnterState(FiniteStateMachine stateMachine);
        void OnEndState();
        void PerformState();
        void ResetState();

        List<Transition> PossibleTransitions { get; }
        void AddTransition(Transition transition);
        void RemoveTransition(Transition transition);
    }
}
