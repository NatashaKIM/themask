﻿using System;
using System.Collections.Generic;
using FSM.States;

namespace FSM
{
    public class FiniteStateMachine
    {
        public FiniteStateMachine(IState startState, IState exitState) 
        {
            this.startState = startState;
            this.currentState = startState;
            this.exitState = exitState;

            TryAddState(startState);
            TryAddState(exitState);
        }
     
        private List<IState> states = new List<IState>();
        private List<Transition> anyStateTransitions = new List<Transition>();

        private IState startState;
        private IState currentState;
        private IState exitState;

        #region Getters
        public IState StartState => startState;
        public IState CurrentState => currentState;
        public IState ExitState => exitState;
        public int StateCount => states.Count;
        #endregion

        public bool TryAddState<T>(T value) where T : IState
        {
            if(!states.Contains(value))
            {
                states.Add(value);
                return true;
            }
            else
            {
                Console.Error.WriteLine("State already has been added.");
                return false;
            }
        }

        public bool TryAddTransitionToAnyState(Transition transition)
        {
            if(!anyStateTransitions.Contains(transition))
            {
                anyStateTransitions.Add(transition);
                return true;
            }
            else
            {
                Console.Error.WriteLine("State already has been added.");
                return false;
            }
        }

        public bool TrySetStateManually<T>(T state) where T : IState
        {   
            if (!states.Contains(state))
            {
                Console.Error.WriteLine("State already has been added.");
                return false;
            }
            else if (state.Equals(currentState))
            { 
                Console.Error.WriteLine("State machine is already in this state.");
                return false;
            }
            else
            {
                currentState = state;
                return true;
            }
        }

        public void UpdateState()
        {
            if(currentState == exitState)
            {
                //Machine finished performing states.
                return;
            }
            else
            {
                TryToMoveToNextState();
                currentState.PerformState();
                //if (currentState == exitState)
                //{ 
                //    //Reached ExitState
                //}
            }
        }

        private bool TryToMoveToNextState()
        {
            foreach(Transition transition in anyStateTransitions)
            {
                if (transition.Condition.Invoke())
                {
                    currentState?.OnEndState();
                    currentState = transition.DestinationState;
                    currentState.OnEnterState(this);
                    return true;
                }
            }

            foreach (Transition transition in currentState.PossibleTransitions)
            {
                if (transition.Condition.Invoke())
                {
                    currentState?.OnEndState();
                    currentState = transition.DestinationState;
                    currentState.OnEnterState(this);
                    return true;
                }
            }
            return false;
        }
    }
}
