﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSM.States
{
    public abstract class BaseState : IState
    {
        protected FiniteStateMachine stateMachine = null;

        protected List<Transition> possibleTransitions = new List<Transition>();
        public List<Transition> PossibleTransitions => possibleTransitions;

        protected Action performStateAction = null;
        public Action PerformStateAction => performStateAction;

        protected Action enterStateAction = null;
        public Action EnterStateAction => enterStateAction;

        protected Action exitStateAction = null;
        public Action ExitStateAction => exitStateAction;

        public virtual void OnEnterState(FiniteStateMachine stateMachine) 
        { 
            this.stateMachine = stateMachine;
            enterStateAction?.Invoke();
        }
        
        public virtual void OnEndState() 
        {
            exitStateAction?.Invoke();
        }

        public virtual void PerformState()
        {
            performStateAction?.Invoke();
        }

        public abstract void ResetState();

        public void AddTransition(Transition transition)
        {
            if (!possibleTransitions.Contains(transition))
            {
                possibleTransitions.Add(transition);
            }
        }

        public void RemoveTransition(Transition transition)
        {
            if (possibleTransitions.Contains(transition))
            {
                possibleTransitions.Remove(transition);
            }
        }

        public BaseState SetEnterStateAction(Action enterStateAction)
        {
            this.enterStateAction = enterStateAction;
            return this;
        }

        public BaseState SetPerformStateAction(Action performStateAction)
        {
            this.performStateAction = performStateAction;
            return this;
        }

        public BaseState SetExitStateAction(Action enterStateAction)
        {
            this.exitStateAction = enterStateAction;
            return this;
        }
    }
}
