﻿using System;
using FSM.States;

namespace FSM
{
    public class Transition
    {
        private IState destination;
        public IState DestinationState => destination;

        private IState source;
        public IState SourceState => source;

        private Func<bool> condition;
        public Func<bool> Condition => condition;

        public Transition(IState source, IState destination, Func<bool> condition)
        {
            this.source = source;
            this.destination = destination;
            this.condition = condition;
        }
        
        public Transition(IState destination, Func<bool> condition)
        {
            this.source = null;
            this.destination = destination;
            this.condition = condition;
        }
    }
}
