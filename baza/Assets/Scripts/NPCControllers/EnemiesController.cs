﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ReikonAI
{
    public class EnemiesController : MonoBehaviour, IUpdatable
    {
        private IEventBus eventBus;
        
        [SerializeField] private List<BaseEnemy> enemies = new List<BaseEnemy>(); 


        public void Init(IEventBus eventBus, IPlayerController playerController)
        {
            this.eventBus = eventBus;

            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].Init(eventBus, playerController);
            }
        }

        public void UpdateComponent()
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].CheckTransitions();
            }
        }
    }
}
