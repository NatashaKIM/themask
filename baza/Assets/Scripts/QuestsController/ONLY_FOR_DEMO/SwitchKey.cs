﻿using System.Collections;
using UnityEngine;

public class SwitchKey : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Weapon"))
        {
            this.gameObject.SetActive(false);
        }
    }
}