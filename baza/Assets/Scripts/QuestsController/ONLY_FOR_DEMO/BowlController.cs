﻿using System.Collections;
using UnityEngine;
using UIViews;

public class BowlController : MonoBehaviour
{
    CatQuestController catQuestController;
    IUIController uIController;

    [SerializeField] private string fountText = "You've found a cat's bowl";

    public void Init(CatQuestController catQuestController, IUIController uIController)
    {
        this.catQuestController = catQuestController;
        this.uIController = uIController;
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player") || catQuestController.foundBowl) return;

        uIController.SetDataAndShowView(new DialogueViewData(fountText, transform, Vector3.up * 2));
        catQuestController.foundBowl = true;
        StartCoroutine(HideCoroutine());
    }

    private IEnumerator HideCoroutine()
    {
        yield return new WaitForSeconds(2f);
        uIController.HideViewByDataType(new DialogueViewData(transform));
        gameObject.SetActive(false);
    }
}