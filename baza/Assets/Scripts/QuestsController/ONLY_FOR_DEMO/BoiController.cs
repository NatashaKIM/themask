﻿using System;
using System.Collections;
using UnityEngine;
using UIViews;
using EventsBus;

public class BoiController : MonoBehaviour
{
    IEventBus eventBus;
    IUIController uIController;

    [SerializeField] private string firstText  = "Have you seen my cat? I can't find him!";
    [SerializeField] private string secondText = "Have you found my cat yet?";
    [SerializeField] private string thirdText  = "Oh, you've found him! Thank you!";
    [SerializeField] private string rewardText = "I hope that this little gift will reward your efforts!";
    [SerializeField] private string lastText   = "Thanks again! It really means a lot to me!";


    [SerializeField] private Vector3 dialogueOffset;
    [SerializeField] private float hideDialogueDelay = 3f;

    [SerializeField] private GameObject cat;

    private CatQuestController catQuestController;
    Coroutine twoTexts = null;
    Coroutine hidingCoroutine = null;

    public void Init(CatQuestController catQuestController, IEventBus eventBus, IUIController uIController)
    {
        this.catQuestController = catQuestController;
        this.eventBus = eventBus;
        this.uIController = uIController;
        cat.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;

        if (catQuestController.questFinished)
        {
            uIController.SetDataAndShowView(new DialogueViewData(lastText, transform, dialogueOffset));
            return;
        }


        var visits = (catQuestController.visitedBoyFirst, catQuestController.visitedCatSecond);
        string textToShow = string.Empty;
        switch (visits)
        {
            case var t when t == (false, false):
                catQuestController.visitedBoyFirst = true;
                textToShow = firstText;
                break;
            case var t when t == (true, false):
                textToShow = secondText;
                break;
            case var t when t == (true, true):
                twoTexts = StartCoroutine(ShowTwoTexts());
                cat.gameObject.SetActive(true);
                return;
        }
        
        uIController.SetDataAndShowView(new DialogueViewData(textToShow, transform, dialogueOffset));
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(twoTexts == null && hidingCoroutine == null)
            hidingCoroutine = StartCoroutine(HideCoroutine());
    }

    private IEnumerator ShowTwoTexts()
    {
        uIController.SetDataAndShowView(new DialogueViewData(thirdText, transform, dialogueOffset));

        yield return new WaitForSeconds(3.5f);

        uIController.SetDataAndShowView(new DialogueViewData(rewardText, transform, dialogueOffset));
        eventBus.InvokeEvent(new OnUnlockDoubleJump());
        catQuestController.questFinished = true;
        yield return new WaitForSeconds(3.5f);
        twoTexts = null;
        hidingCoroutine = StartCoroutine(HideCoroutine());
    }

    private IEnumerator HideCoroutine()
    {
        yield return new WaitForSeconds(hideDialogueDelay);
        uIController.HideViewByDataType(new DialogueViewData(transform));
        hidingCoroutine = null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position + dialogueOffset, 0.1f);
    }
}
