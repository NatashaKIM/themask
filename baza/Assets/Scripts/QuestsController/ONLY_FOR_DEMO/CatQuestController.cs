using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventsBus;
using UIViews;

public class CatQuestController : MonoBehaviour
{
    IEventBus eventBus;
    IUIController uIController;

    public bool visitedBoyFirst; 
    public bool visitedBoySecond; 
    public bool visitedCatFirst; 
    public bool visitedCatSecond;
    public bool foundBowl;
    public bool questFinished;

    [SerializeField] private BoiController boiController;
    [SerializeField] private CatController catController;
    [SerializeField] private BowlController bowlController;

    public void Init(IEventBus eventBus, IUIController uIController)
    {
        this.eventBus = eventBus;
        this.uIController = uIController;

        boiController.Init(this, eventBus, uIController);
        catController.Init(this, eventBus, uIController);
        bowlController.Init(this, uIController);


        visitedBoyFirst = false;
        visitedBoySecond = false;
        visitedCatFirst = false;
        visitedCatSecond = false;
        foundBowl = false;
        questFinished = false;
}
}
