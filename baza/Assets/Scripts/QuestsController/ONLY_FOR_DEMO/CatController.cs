﻿using System.Collections;
using UnityEngine;
using UIViews;
using EventsBus;

public class CatController : MonoBehaviour
{
    IEventBus eventBus;
    IUIController uIController;

    [SerializeField] private string firstText = "Go away! I hate people! They left me here!";
    [SerializeField] private string secondText = "You still here? Go away!";
    [SerializeField] private string thirdText = "Oh, you've found my bowl! So they haven't left me?";
    [SerializeField] private string lastText = "Please, bring me to my family!";

    [SerializeField] private Vector3 dialogueOffset;
    [SerializeField] private float hideDialogueDelay = 3f;

    private CatQuestController catQuestController;
    Coroutine twoTexts = null;

    public void Init(CatQuestController catQuestController, IEventBus eventBus, IUIController uIController)
    {
        this.catQuestController = catQuestController;
        this.eventBus = eventBus;
        this.uIController = uIController;
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player") || catQuestController.questFinished) return;

        if (catQuestController.visitedCatSecond)
        {
            uIController.SetDataAndShowView(new DialogueViewData(lastText, transform, dialogueOffset));
            return;
        }

        var visits = (catQuestController.visitedCatFirst, catQuestController.foundBowl);
        string textToShow = string.Empty;
        switch (visits)
        {
            case var t when t == (false, false):
                catQuestController.visitedCatFirst = true;
                textToShow = firstText;
                break;
            case var t when t == (true, false):
                textToShow = secondText;
                break;
            case var t when t == (true, true):
            case var c when c == (false, true):
                twoTexts = StartCoroutine(ShowTwoTexts());
                catQuestController.visitedCatSecond = true;
                return;
        }
        
        uIController.SetDataAndShowView(new DialogueViewData(textToShow, transform, dialogueOffset));
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(twoTexts == null)
            StartCoroutine(HideCoroutine());
    }

    private IEnumerator ShowTwoTexts()
    {
        uIController.SetDataAndShowView(new DialogueViewData(thirdText, transform, dialogueOffset));

        yield return new WaitForSeconds(3.5f);

        uIController.SetDataAndShowView(new DialogueViewData(lastText, transform, dialogueOffset));
        yield return new WaitForSeconds(2.5f);

        yield return new WaitForSeconds(hideDialogueDelay);
        uIController.HideViewByDataType(new DialogueViewData(transform));
        gameObject.SetActive(false);
    }

    private IEnumerator HideCoroutine()
    {
        yield return new WaitForSeconds(hideDialogueDelay);
        uIController.HideViewByDataType(new DialogueViewData(transform));
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position + dialogueOffset, 0.1f);
    }
}
