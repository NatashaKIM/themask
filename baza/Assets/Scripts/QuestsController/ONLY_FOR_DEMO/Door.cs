using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public List<GameObject> switches = new List<GameObject>();

    // Update is called once per frame
    void Update()
    {
        if(AreAllSwitchesDisabled())
        {
            this.gameObject.SetActive(false);
        }
    }

    bool AreAllSwitchesDisabled()
    {
        for (int i = 0; i < switches.Count; i++)
        {
            if (switches[i].activeSelf) return false;
        }

        return true;
    }
}
