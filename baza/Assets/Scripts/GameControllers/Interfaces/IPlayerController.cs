﻿using UnityEngine;

public interface IPlayerController
{
    Transform transform { get; }
}