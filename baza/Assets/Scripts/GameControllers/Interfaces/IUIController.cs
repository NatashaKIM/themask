﻿using UIViews;

public interface IUIController
{
    /// <summary>
    /// Shows a specific view based on given data
    /// </summary>
    /// <param name="viewData">Data to show</param>
    void SetDataAndShowView(IViewData viewData);
    void HideViewByDataType(IViewData viewData);
    void UpdateData(IViewData viewData);
}
