﻿public interface IUpdatable
{
    void UpdateComponent();
}
