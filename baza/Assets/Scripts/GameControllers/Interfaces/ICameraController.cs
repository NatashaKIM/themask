﻿using UnityEngine;

public interface ICameraController
{
    Camera Camera { get; }
    void AddObjectToFollow(Transform objectToFollow);
    void RemoveObjectToFollow(Transform objectToFollow);
    void SwapXOffset();
}