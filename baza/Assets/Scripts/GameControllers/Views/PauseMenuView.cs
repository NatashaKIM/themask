﻿using System;
using System.Collections.Generic;
using UIViews;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UIViews
{
    public class PauseMenuView : MonoBehaviour, IView
    {
        [SerializeField] private Button resumeButton;
        [SerializeField] private TMP_Text text;


        public void HideView()
        {
            this.gameObject.SetActive(false);
        }

        public void SetData(IViewData data)
        {
            if(((PauseMenuViewData)data).isDeathScreen)
            {
                resumeButton.gameObject.SetActive(false);
                text.text = "You died!";
            }
            else
            {
                resumeButton.gameObject.SetActive(true);
                text.text = "Pause";
            }
        }

        public void ShowView()
        {
            this.gameObject.SetActive(true);
        }
    }
}
