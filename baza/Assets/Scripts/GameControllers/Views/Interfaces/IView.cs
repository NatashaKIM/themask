﻿namespace UIViews
{
    public interface IView
    {
        void ShowView();
        void HideView();
        void SetData(IViewData data);
    }
}
