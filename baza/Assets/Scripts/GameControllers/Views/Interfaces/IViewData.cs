﻿namespace UIViews
{
    public interface IViewData
    {
        string TranslationKey { get; }
    }
}
