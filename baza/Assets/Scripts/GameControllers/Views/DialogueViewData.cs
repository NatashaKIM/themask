﻿using UnityEngine;

namespace UIViews
{
    public struct DialogueViewData : IViewData
    {
        public string TranslationKey{ get; private set; }
        public Transform FollowedTransform { get; private set; }
        public Vector3 Offset { get; private set; }

        public DialogueViewData(string translationKey, Transform talkingNPCTransform, Vector3 offset)
        {
            TranslationKey = translationKey;
            FollowedTransform = talkingNPCTransform;
            Offset = offset;
        }
        
        public DialogueViewData(string translationKey, Transform talkingNPCTransform)
        {
            TranslationKey = translationKey;
            FollowedTransform = talkingNPCTransform;
            Offset = Vector3.zero;
        }
        
        /// <summary>
        /// USE THIS ONLY FOR ON DIALOGUE ENDED EVENT!
        /// </summary>
        public DialogueViewData(Transform talkingNPCTransform)
        {
            FollowedTransform = talkingNPCTransform;
            TranslationKey = string.Empty;
            Offset = Vector3.zero;
        }
    }
}
