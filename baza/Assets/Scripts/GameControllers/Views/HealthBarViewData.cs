﻿using System;
using UnityEngine;

namespace UIViews
{
    public struct HealthBarViewData : IViewData
    {
        public string TranslationKey => string.Empty;

        public readonly float currentHealth01;
        public readonly bool isSpiritWorld;

        public HealthBarViewData(float currentHealth01, bool isSpiritWorld)
        {
            this.currentHealth01 = currentHealth01;
            this.isSpiritWorld = isSpiritWorld;
        }
    }
}
