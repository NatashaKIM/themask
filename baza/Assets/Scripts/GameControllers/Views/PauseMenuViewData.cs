﻿using System;

namespace UIViews
{
    public struct PauseMenuViewData : IViewData
    {
        public string TranslationKey => string.Empty;
        public bool isDeathScreen;

        public PauseMenuViewData(bool isDeathScreen)
        {
            this.isDeathScreen = isDeathScreen;
        }
    }
}
