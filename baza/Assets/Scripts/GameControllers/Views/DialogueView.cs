﻿using System.Collections;
using UnityEngine;
using TMPro;

namespace UIViews
{
    public class DialogueView : MonoBehaviour, IView
    {
        [SerializeField] private TMP_Text dialogueText;
        [SerializeField] private float showingTimeInMilliseconds = 50f;

        private string textToShow;
        private Transform followedTransform;
        private Vector3 offset;
        private Canvas canvas;

        public void ShowView()
        {
            this.gameObject.SetActive(true);
            canvas = GetComponentInParent<Canvas>();
        }

        public void HideView()
        {
            this.gameObject.SetActive(false);
        }

        public void SetData(IViewData viewData)
        {
            DialogueViewData data = (DialogueViewData)viewData;

            this.textToShow = data.TranslationKey;
            this.followedTransform = data.FollowedTransform;
            this.offset = data.Offset;

            StartCoroutine(UpdatingCoroutine());
            StartCoroutine(UpdateText());
        }

        private IEnumerator UpdateText()
        {
            dialogueText.text = string.Empty;

            WaitForSeconds delay = new WaitForSeconds(showingTimeInMilliseconds / 1000);

            for (int i = 0; i < textToShow.Length; i++)
            {
                dialogueText.text += textToShow[i];
                yield return delay;
            }
        }

        private IEnumerator UpdatingCoroutine()
        {
            while (gameObject.activeSelf)
            {
                UpdatePosition();
                yield return null;
            }
        }

        private void UpdatePosition()
        {
            Vector3 screenPos = Camera.main.WorldToScreenPoint(followedTransform.position + offset);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, screenPos, canvas.worldCamera, out Vector2 pos);
            transform.position = canvas.transform.TransformPoint(pos);
        }
    }
}