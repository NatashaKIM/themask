﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UIViews
{
    public class HealthBarView : MonoBehaviour, IView
    {
        [SerializeField] private Color spiritColor;
        [SerializeField] private Color humanColor;
        [SerializeField] private Image healthSlider;
        [SerializeField] private Sprite maskEnabled;
        [SerializeField] private Sprite maskDisabled;
        [SerializeField] private Image currentMask;

        private float maxwidth;

        private void Awake()
        {
            maxwidth = (healthSlider.transform as RectTransform).sizeDelta.x;
            healthSlider.color = humanColor;
            currentMask.sprite = maskDisabled;
        }

        public void SetData(IViewData data)
        {
            HealthBarViewData healthBarViewData = (HealthBarViewData)data;

            RectTransform t = healthSlider.transform as RectTransform;
            t.sizeDelta = new Vector2(healthBarViewData.currentHealth01 * maxwidth, t.sizeDelta.y);
            healthSlider.color = healthBarViewData.isSpiritWorld ? spiritColor : humanColor;
            currentMask.sprite = healthBarViewData.isSpiritWorld ? maskEnabled : maskDisabled;
        }

        public void ShowView()
        {
            this.gameObject.SetActive(true);
        }
        
        public void HideView()
        {
            this.gameObject.SetActive(false);
        }
    }
}
