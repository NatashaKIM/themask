﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using EventsBus;

public class CameraController : MonoBehaviour, IUpdatable, ICameraController, IListener<OnEngage>, IListener<OnDisengage>,
    IListener<OnDialogueStarted>, IListener<OnDialogueEnded>
{
    private IEventBus eventBus;

    [SerializeField] private new Camera camera;
    [SerializeField] private float followSpeed = 1f;
    [SerializeField] private float smoothMoveTime = 0.3f;
    [SerializeField] private Transform objectToFollow;
    [SerializeField] private float minZoom = 15f;
    [SerializeField] private float maxZoom = 40f;
    [SerializeField] private float maxDistance = 40f;
    [SerializeField] private float zoomLerpSpeed = 5f;
    [SerializeField] private float smoothZoomTime = 0.3f;
    [SerializeField] private Vector3 offset;
    
    private List<Transform> followedObjects = new List<Transform>();

    private Vector3 moveVel = new Vector3();
    private float zoomVel = 0;

    public Camera Camera => camera;

    public void Init(IPlayerController player, IEventBus eventBus)
    {
        this.eventBus = eventBus;
        AddObjectToFollow(player.transform);

        eventBus.RegisterListener<OnEngage>(this);
        eventBus.RegisterListener<OnDisengage>(this);
        eventBus.RegisterListener<OnDialogueStarted>(this);
        eventBus.RegisterListener<OnDialogueEnded>(this);
    }

    public void UpdateComponent()
    {
        if (followedObjects.Count <= 0) return;

        Move();
        Zoom();
    }

    private void Move()
    {
        Vector3 centerPoint = GetCenterPoint();

        if (followedObjects.Count < 2) centerPoint += offset; // Add offset if less than two object followed

        this.transform.position = Vector3.SmoothDamp(this.transform.position, centerPoint, ref moveVel, smoothMoveTime, followSpeed);
    }

    private void Zoom()
    {
        float greatestDistance = GetGreatestDistance();

        greatestDistance = Mathf.Clamp(greatestDistance, 0, maxDistance);
        var t = greatestDistance.Map(0, maxDistance, minZoom, maxZoom);

        camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, t, ref zoomVel, smoothZoomTime, zoomLerpSpeed);
    }

    private Vector3 GetCenterPoint()
    {
        if (followedObjects.Count == 1)
        {
            return followedObjects[0].position;
        }

        Bounds bounds = new Bounds(followedObjects[0].position, Vector3.zero);

        for (int i = 0; i < followedObjects.Count; i++)
        {
            bounds.Encapsulate(followedObjects[i].position);
        }

        return bounds.center;
    }

    private float GetGreatestDistance()
    {
        float greatestDistance = 0;

        for (int i = 0; i < followedObjects.Count - 1; i++)
        {
            float distance = Vector3.Distance(followedObjects[i].position, followedObjects[i + 1].position);
            if (distance > greatestDistance)
                greatestDistance = distance;
        }

        return greatestDistance;
    }

    public void AddObjectToFollow(Transform objectToFollow)
    {
        if (objectToFollow != null && !followedObjects.Contains(objectToFollow))
            followedObjects.Add(objectToFollow);
    }

    public void RemoveObjectToFollow(Transform objectToFollow)
    {
        if (objectToFollow != null && followedObjects.Contains(objectToFollow))
            followedObjects.Remove(objectToFollow);
    }

    public void SwapXOffset()
    {
        offset.x = -offset.x;
    }

    public void OnEvent(OnEngage data)
    {
        AddObjectToFollow(data.enemy.transform);
    }   
    
    public void OnEvent(OnDisengage data)
    {
        RemoveObjectToFollow(data.enemy.transform);
    }

    public void OnEvent(OnDialogueStarted data)
    {
        AddObjectToFollow(data.talkingNPC);
    }   
    
    public void OnEvent(OnDialogueEnded data)
    {
        RemoveObjectToFollow(data.talkingNPC);
    }
}
