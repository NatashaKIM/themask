﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventsBus;

public class MenuState : BaseState
{
    public MenuState(Injector injector) : base(injector) { }

    public override void InitState()
    {
        injector.EventBus.InvokeEvent(new OnStateInitialized(this));
        injector.CameraController.gameObject.SetActive(false);
        injector.GameEnvironment.SetActive(false);
        injector.MenuEnvironment.SetActive(true);
    }
    public override void UpdateState()
    {

    }

    public override void DeinitState()
    {
        injector.EventBus.InvokeEvent(new OnStateDeinitialized(this));
    }
}
