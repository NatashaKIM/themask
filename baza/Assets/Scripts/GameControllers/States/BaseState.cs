using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventsBus;

public abstract class BaseState
{
    protected Injector injector;

    public BaseState(Injector injector) => this.injector = injector;

    public abstract void InitState();
    public abstract void UpdateState();
    public abstract void DeinitState();
}
