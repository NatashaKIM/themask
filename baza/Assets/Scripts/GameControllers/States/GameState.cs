﻿using EventsBus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : BaseState
{
    public GameState(Injector injector) : base(injector) { }

    public override void InitState()
    {
        injector.EventBus.InvokeEvent(new OnStateInitialized(this));
        injector.CameraController.gameObject.SetActive(true);
        injector.GameEnvironment.SetActive(true);
        injector.MenuEnvironment.SetActive(false);
        injector.UIController.HealthBarView.ShowView();
    }
    public override void UpdateState()
    {
        injector.CameraController.UpdateComponent();
        injector.PlayerController.UpdateComponent();
        injector.EnemiesController.UpdateComponent();
    }
    public override void DeinitState()
    {
        injector.EventBus.InvokeEvent(new OnStateDeinitialized(this));
    }
}
