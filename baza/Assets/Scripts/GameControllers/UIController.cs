﻿using System.Collections;
using UnityEngine;
using UIViews;
using System;
using EventsBus;

public class UIController : MonoBehaviour, IUIController
{
    private IEventBus eventBus; 

    [SerializeField] private DialogueView dialogueView;
    public DialogueView DialogueView => dialogueView;

    [SerializeField] private PauseMenuView pauseMenuView;
    public PauseMenuView PauseMenuView => pauseMenuView;

    [SerializeField] private HealthBarView healthBarView;
    public HealthBarView HealthBarView => healthBarView;

    public void Init(IEventBus eventBus) => this.eventBus = eventBus;

    public void SetDataAndShowView(IViewData viewData)
    {
        switch (viewData)
        {
            case DialogueViewData data:
                dialogueView.ShowView();
                dialogueView.SetData(data);
                eventBus.InvokeEvent(new OnDialogueStarted(data.FollowedTransform));
                break;
            case PauseMenuViewData data:
                pauseMenuView.ShowView();
                pauseMenuView.SetData(data);
                break;
        }
    }

    public void HideViewByDataType(IViewData viewType)
    {
        switch (viewType)
        {
            case DialogueViewData data:
                dialogueView.HideView();
                eventBus.InvokeEvent(new OnDialogueEnded(data.FollowedTransform));
                break;
            case PauseMenuViewData data:
                pauseMenuView.HideView();
                break;
        }
    }

    public void UpdateData(IViewData viewData)
    {
        switch (viewData)
        {
            case DialogueViewData data:
                dialogueView.SetData(data);
                break;
            case HealthBarViewData data:
                healthBarView.SetData(data);
                break;
        }
    }

}