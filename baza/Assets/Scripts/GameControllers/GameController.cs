using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EventsBus;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private Injector injector;

    [SerializeField] private AudioSource gameAudioSource;
    [SerializeField] private AudioSource menuAudioSource;
    [SerializeField] private AudioClip menuClip;
    [SerializeField] private AudioClip gameClip;
    [SerializeField] private GameObject credits;

    bool paused = false;
    BaseState currentState;

    void Awake()
    {
        injector.Init();
        ChangeState(States.MenuState); // FOR TESTING PURPOSE
    }

    // Update is called once per frame
    void Update()
    {
        if(currentState != null && !paused)
        {
            currentState.UpdateState();
        }

        if(currentState is GameState)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Tab)) && !paused)
            {
                injector.UIController.SetDataAndShowView(new UIViews.PauseMenuViewData());
                paused = true;
                injector.PlayerController.CanMove = false;
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Tab)) && (paused || !injector.PlayerController.died))
            {
                injector.UIController.HideViewByDataType(new UIViews.PauseMenuViewData());
                injector.PlayerController.CanMove = true;
                paused = false;
            }
            else if (injector.PlayerController.died)
            {
                injector.UIController.SetDataAndShowView(new UIViews.PauseMenuViewData(true));
                paused = true;
                injector.PlayerController.CanMove = false;
            }
        }
        else 
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Tab))
            {
                credits.gameObject.SetActive(false);
            }
        }
    }

    public void ChangeState(States newState)
    {
        currentState?.DeinitState();

        switch (newState)
        {
            case States.GameState:
                currentState = new GameState(injector);
                StartCoroutine(ChangeMusic(gameAudioSource, menuAudioSource));
                break;
            case States.MenuState:
                currentState = new MenuState(injector);
                StartCoroutine(ChangeMusic(menuAudioSource, gameAudioSource));
                break;
        }
        currentState.InitState();
    }

    public enum States
    {
        MenuState,
        GameState
    }

    // demo only
    private IEnumerator ChangeMusic(AudioSource enablingAudioSource, AudioSource disablingAudioSource)
    {
        while(disablingAudioSource.volume > 0)
        {
            disablingAudioSource.volume -= 0.05f;
            yield return 0;
        }

        disablingAudioSource.Stop();
        enablingAudioSource.Play();

        while(enablingAudioSource.volume < 1)
        {
            enablingAudioSource.volume += 0.05f;
            yield return 0;
        }

    }

    public void StartGame()
    {
        ChangeState(States.GameState);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ResumeFromPause()
    {
        injector.UIController.HideViewByDataType(new UIViews.PauseMenuViewData());
        paused = false;
        injector.PlayerController.CanMove = true;
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
