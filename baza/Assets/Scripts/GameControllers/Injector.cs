﻿using System;
using UnityEngine;
using EventsBus;
using ReikonAI;
using System.Collections.Generic;
using System.Reflection;

public class Injector : MonoBehaviour
{
#region MonoBehaviours and other important controllers
    public EventBus EventBus { get; private set; }

    [SerializeField] private InputHandler inputHandler;
    public InputHandler InputHandler { get { return inputHandler; } }

    [SerializeField] private PlayerController player;
    public PlayerController PlayerController { get { return player; } }

    [SerializeField] private CameraController cameraController;
    public CameraController CameraController { get { return cameraController; } }

    [SerializeField] private EnemiesController enemiesController;
    public EnemiesController EnemiesController { get { return enemiesController; } }

    [SerializeField] private UIController uIController;
    public UIController UIController { get { return uIController; } }

    [SerializeField] private CatQuestController catQuestController;
    public CatQuestController CatQuestController { get { return catQuestController; } }

    [SerializeField] private GameObject gameEnviro;
    public GameObject GameEnvironment { get { return gameEnviro; } }

    [SerializeField] private GameObject menuEnviro;
    public GameObject MenuEnvironment { get { return menuEnviro; } }
#endregion
#region Injection
    private Dictionary<Type, object> d = new Dictionary<Type, object>();

    public void Init()
    {
        this.EventBus = new EventBus();
        SetReference(typeof(IEventBus), EventBus);
        SetReference(typeof(IInputHandler), inputHandler);
        SetReference(typeof(IPlayerController), player);
        SetReference(typeof(ICameraController), cameraController);
        SetReference(typeof(IUIController), uIController);

        Inject(player);
        Inject(cameraController);
        Inject(enemiesController);
        Inject(uIController);
        Inject(catQuestController);
    }

    public void SetReference(Type t, object o)
    {
        d.Add(t, o);
    }

    public void Inject(object o)
    {
        Type type = o.GetType();
        MethodInfo method = type.GetMethod("Init");
        ParameterInfo[] parameters = method.GetParameters();
        object[] refs = new object[parameters.Length];


        for (int i = 0; i < refs.Length; i++)
        {
            Type parameterType = parameters[i].ParameterType;
            if (d.ContainsKey(parameterType))
            {
                refs[i] = d[parameterType];
            }
            else
            {
                Debug.LogError("Reference not set for type: " + parameterType);
                return;
            }
        }
        method.Invoke(o, refs);
    }
#endregion
}