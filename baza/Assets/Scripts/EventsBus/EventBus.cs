﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;

namespace EventsBus
{
    public class EventBus : IEventBus
    {
        private Dictionary<Type, HashSet<IListener>> listeners = new Dictionary<Type, HashSet<IListener>>();

        public void RegisterListener<TEvent>(IListener<TEvent> listener) where TEvent : IEvent
        {
            if (listeners.ContainsKey(typeof(TEvent)))
            {
                listeners[typeof(TEvent)].Add(listener);
            }
            else
            {
                listeners.Add(typeof(TEvent), new HashSet<IListener>());
                listeners[typeof(TEvent)].Add(listener);
            }
        }
        
        public void UnregisterListener<TEvent>(IListener<TEvent> listener) where TEvent : IEvent
        {
            if (listeners.ContainsKey(typeof(TEvent)))
            {
                listeners[typeof(TEvent)].Remove(listener);
            }
            else 
            {
                UnityEngine.Debug.LogErrorFormat("Listener is not registered: {0}", listener);
            }
        }

        public void InvokeEvent<TEvent>(TEvent eventData) where TEvent : IEvent
        {
            if (!listeners.ContainsKey(typeof(TEvent)))
            {
                listeners.Add(typeof(TEvent), new HashSet<IListener>());
            }

            HashSet<IListener> eventListeners = listeners[typeof(TEvent)];
            for (int i = eventListeners.Count - 1; i >= 0; i--)
            {
                (eventListeners.ElementAt(i) as IListener<TEvent>).OnEvent(eventData);
            }
            UnityEngine.Debug.LogFormat("Event invoked: {0}", typeof(TEvent));
        }
    }
}
