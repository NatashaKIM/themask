﻿using EventsBus;

public interface IEventBus
{
    public void RegisterListener<TEvent>(IListener<TEvent> listener) where TEvent : IEvent;
    public void UnregisterListener<TEvent>(IListener<TEvent> listener) where TEvent : IEvent;
    public void InvokeEvent<TEvent>(TEvent eventData) where TEvent : IEvent;
}
