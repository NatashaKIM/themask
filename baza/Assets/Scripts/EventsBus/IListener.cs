﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsBus
{
    public interface IListener
    {

    }

    public interface IListener<T> : IListener where T : IEvent
    {
        void OnEvent(T data);
    }
}
