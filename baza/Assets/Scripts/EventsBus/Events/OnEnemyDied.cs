﻿using ReikonAI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsBus
{
    public class OnEnemyDied : Event
    {
        public readonly BaseEnemy baseEnemy;

        public OnEnemyDied(BaseEnemy baseEnemy)
        {
            this.baseEnemy = baseEnemy;
        }
    }
}
