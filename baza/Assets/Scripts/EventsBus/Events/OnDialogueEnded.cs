﻿using UnityEngine;

namespace EventsBus
{
    public class OnDialogueEnded : Event
    {
        public Transform talkingNPC;

        public OnDialogueEnded(Transform talkingNPC)
        {
            this.talkingNPC = talkingNPC;
        }
    }
}