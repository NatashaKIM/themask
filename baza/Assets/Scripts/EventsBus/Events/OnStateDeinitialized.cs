﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsBus
{
    public class OnStateDeinitialized : IEvent
    {
        public BaseState deinitializedState;

        public OnStateDeinitialized(BaseState stateInitialized)
        {
            this.deinitializedState = stateInitialized;
        }
    }
}
