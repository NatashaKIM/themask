﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace EventsBus
{
    public class OnWorldsChanged : IEvent
    {
        public World CurrentWorld { get; private set; }
        public Camera MainCamera { get; private set; }

        public OnWorldsChanged(World currentWorld)
        {
            this.CurrentWorld = currentWorld;
            this.MainCamera = Camera.main;
        }
    }
}
