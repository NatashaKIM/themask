﻿using UnityEngine;

namespace EventsBus
{
    public class OnDialogueStarted : Event
    {
        public Transform talkingNPC;

        public OnDialogueStarted(Transform talkingNPC)
        {
            this.talkingNPC = talkingNPC;
        }
    }
}