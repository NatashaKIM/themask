﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsBus
{
    public class OnStateInitialized : Event
    {
        public BaseState initializedState;

        public OnStateInitialized(BaseState stateInitialized)
        {
            this.initializedState = stateInitialized;
        }
    }
}
